package org.izhnet.zimu

import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.filters.SdkSuppress
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import com.android21buttons.fragmenttestrule.FragmentTestRule
import io.mockk.every
import io.mockk.mockk
import org.hamcrest.CoreMatchers.not
import org.izhnet.zimu.dagger.DaggerGridComponent
import org.izhnet.zimu.dagger.GridComponent
import org.izhnet.zimu.dagger.module.GridModule
import org.izhnet.zimu.dagger.module.MainModule
import org.izhnet.zimu.model.data.DbItem
import org.izhnet.zimu.viewmodel.ImgGridVM
import org.izhnet.zimu.viewmodel.MainVM
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.File

/**
 * Тесты для [ImgGridFr]
 */
@SdkSuppress(minSdkVersion = 18)
class ImgGridFrTest {
    @get:Rule
    val rule: FragmentTestRule<*, ImgGridFr> = FragmentTestRule.create(ImgGridFr::class.java,
                                                                       false, false)
    private lateinit var fragment: ImgGridFr
    private val file = File.createTempFile("test", ".png")
    private val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    private val liveData = MutableLiveData<List<DbItem>>()
    private val frVM = mockk<ImgGridVM>(relaxed = true).also {
        every { it.getLiveData() } returns liveData
        every { it.getSize() } answers { liveData.value?.size ?: 0 }
        every { it.createUriFromFile(any()) } returns Uri.EMPTY
    }
    private val navigator = mockk<NavController>(relaxed = true)
    private val actVM = mockk<MainVM>(relaxed = true)
    private val frVMFactory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return frVM as T
            }
    }
    private val actVMFactory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return actVM as T
            }
    }
    private val gridComponent = buildGridComponent()

    @Before
    fun setUp() {
        App.INSTANCE.gridComponent = gridComponent
        fragment = ImgGridFr()
        rule.launchFragment(fragment)
        fragment.navigator = navigator
    }

    @After
    fun tearDown() {
        rule.finishActivity()
    }

    @Test
    fun checkUIElements() {
        onView(withId(R.id.rv_image_grid)).check(matches(isDisplayed()))
        onView(withId(R.id.fab_camera)).check(matches(isDisplayed()))
        onView(withId(R.id.fab_gallery)).check(matches(isDisplayed()))
    }

    @Test
    fun testImageGrid() {
        liveData.postValue(listOf(DbItem("", "1", "", "", "")))
        onView(withId(R.id.rv_image_grid))
            .perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        liveData.postValue(listOf(DbItem("", "1", "", "", ""), DbItem("", "2", "", "", "")))
        onView(withId(R.id.rv_image_grid))
            .perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
    }

    @Test
    fun testGalleryButton() {
        Intents.init()
        onView(withId(R.id.fab_gallery)).perform(click())
        Intents.intended(IntentMatchers.hasAction(Intent.ACTION_PICK))
        Intents.release()
        device.pressBack()
    }

    @Test
    fun testCameraButtonOK() {
        Intents.init()
        every { frVM.createImageFile() } returns file
        onView(withId(R.id.fab_camera)).perform(click())
        Intents.intended(IntentMatchers.hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        Intents.release()
        device.pressBack()
    }

    @Test
    fun testCameraButtonErr() {
        every { frVM.createImageFile() } returns null
        onView(withId(R.id.fab_camera)).perform(click())
        onView(withText("Can't create photo file"))
                .inRoot(withDecorView(not(rule.activity.window.decorView)))
                .check(matches(isDisplayed()))
    }

    private fun buildGridComponent(): GridComponent {
        return DaggerGridComponent.builder()
            .appComponent(mockk(relaxed = true))
            .mainModule(mockk<MainModule>(relaxed = true).also {
                every { it.provideActFact(any()) } returns actVMFactory
            })
            .gridModule(mockk<GridModule>(relaxed = true).also {
                every { it.provideGridFact(any(), any()) } returns frVMFactory
            })
            .build()
    }
}
