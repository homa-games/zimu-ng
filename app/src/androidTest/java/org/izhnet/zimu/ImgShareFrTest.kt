package org.izhnet.zimu

import android.widget.EditText
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.android21buttons.fragmenttestrule.FragmentTestRule
import io.mockk.every
import io.mockk.mockk
import org.hamcrest.CoreMatchers.not
import org.izhnet.zimu.dagger.DaggerShareComponent
import org.izhnet.zimu.dagger.ShareComponent
import org.izhnet.zimu.dagger.module.MainModule
import org.izhnet.zimu.model.data.DbItem
import org.izhnet.zimu.viewmodel.MainVM
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Тесты для [ImgShareFr]
 */
class ImgShareFrTest {
    @get:Rule
    val rule: FragmentTestRule<*, ImgShareFr> = FragmentTestRule.create(ImgShareFr::class.java,
                                                                        false, false)
    private lateinit var fragment: ImgShareFr
    private val item = DbItem("type", "hash", "url", "domain", "delete")
    private var actVM = mockk<MainVM>(relaxed = true)
        .also { every { it.item } returns item }
    private val actVMFactory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return actVM as T
            }
    }
    private val shareComponent = buildShareComponent()

    @Before
    fun setUp() {
        App.INSTANCE.shareComponent = shareComponent
        fragment = ImgShareFr()
        rule.launchFragment(fragment)
    }

    @After
    fun tearDown() {
        rule.finishActivity()
    }

    @Test
    fun checkUIElements() {
        onView(withId(R.id.iv_image)).check(matches(isDisplayed()))
        onView(withId(R.id.et_comment)).check(matches(isDisplayed()))
        onView(withId(R.id.fab_buffer)).check(matches(isDisplayed()))
    }

    @Test
    fun testComment() {
        val text = "test comment"
        val v = rule.activity.findViewById(R.id.et_comment) as EditText
        onView(withId(R.id.et_comment)).perform(typeText(text))
        assertEquals(text, v.text.toString())
    }

    @Test
    fun testCopyToClipboard() {
        onView(withId(R.id.fab_buffer)).perform(click())
        onView(withText(R.string.copy_to_buffer_msg))
                .inRoot(withDecorView(not(rule.activity.window.decorView)))
                .check(matches(isDisplayed()))
    }

    private fun buildShareComponent(): ShareComponent {
        return DaggerShareComponent.builder()
            .appComponent(mockk(relaxed = true))
            .mainModule(mockk<MainModule>(relaxed = true).also {
                every { it.provideActFact(any()) } returns actVMFactory
            })
            .build()
    }
}
