package org.izhnet.zimu

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.rule.ActivityTestRule
import io.mockk.every
import io.mockk.mockk
import org.izhnet.zimu.dagger.AppComponent
import org.izhnet.zimu.dagger.DaggerMainComponent
import org.izhnet.zimu.dagger.MainComponent
import org.izhnet.zimu.dagger.module.MainModule
import org.izhnet.zimu.model.data.DbItem
import org.izhnet.zimu.viewmodel.MainVM
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Тесты для [MainActivity]
 */
class MainActivityTest {
    @get:Rule
    val rule = ActivityTestRule<MainActivity>(MainActivity::class.java, false, false)

    private val viewModel = mockk<MainVM>(relaxed = true).also {
            every { it.imgPath } returns ""
            every { it.item } returns DbItem("", "", "", "", "")
    }
    private val actVMFactory = object : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return viewModel as T
        }
    }
    private val mainComponent = buildMainComponent()

    @Before
    fun setUp() {
        App.INSTANCE.mainComponent = mainComponent
        rule.launchActivity(null)
    }

    @After
    fun tearDown() {
        rule.finishActivity()
    }

    @Test
    fun testNavigation() {
        val navigator = Navigation.findNavController(rule.activity, R.id.nav_host_fragment)
        //после старта активити отображается ImgGridFr с сеткой загруженных картинок
        Espresso.onView(ViewMatchers.withId(R.id.rv_image_grid))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        //экран загрузки новой картинки. есть предпросмотр загружаемой картинки
        navigator.navigate(R.id.imgUploadFr)
        Espresso.onView(ViewMatchers.withId(R.id.iv_image))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        //экран расшаривания ссылки на картинку. есть поле ввода комментария к ссылке
        navigator.navigate(R.id.imgShareFr)
        Espresso.onView(ViewMatchers.withId(R.id.et_comment))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        //возврат на экран с загруженными картинками
        navigator.navigate(R.id.imgGridFr)
        Espresso.onView(ViewMatchers.withId(R.id.rv_image_grid))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    private fun buildMainComponent(): MainComponent {
        return DaggerMainComponent.builder()
            .appComponent(mockk<AppComponent>(relaxed = true))
            .mainModule(mockk<MainModule>(relaxed = true).also {
                every { it.provideActFact(any()) } returns actVMFactory
            })
            .build()
    }
}
