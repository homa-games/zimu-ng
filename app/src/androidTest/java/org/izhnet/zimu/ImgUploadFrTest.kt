package org.izhnet.zimu

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.android21buttons.fragmenttestrule.FragmentTestRule
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.hamcrest.CoreMatchers.not
import org.izhnet.zimu.dagger.DaggerUploadComponent
import org.izhnet.zimu.dagger.UploadComponent
import org.izhnet.zimu.dagger.module.MainModule
import org.izhnet.zimu.dagger.module.UploadModule
import org.izhnet.zimu.viewmodel.ImgUploadVM
import org.izhnet.zimu.viewmodel.MainVM
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Тесты для [ImgUploadFr]
 */
class ImgUploadFrTest {
    @get:Rule
    val rule: FragmentTestRule<*, ImgUploadFr> = FragmentTestRule.create(ImgUploadFr::class.java,
                                                                         false, false)
    //todo почему-то с этим правилом валится на том, что liveData.postValue() не может быть вызван в фоновом потоке
//    @get:Rule
//    var taskRule = InstantTaskExecutorRule()

    private lateinit var fragment: ImgUploadFr
    private val imgPath = "android.resource://org.izhnet.zimu/" + R.drawable.ic_launcher_foreground
    private val liveData = MutableLiveData<String>()
    private val frVM = mockk<ImgUploadVM>(relaxed = true)
        .also { every { it.getLiveData() } returns liveData }
    private val actVM = mockk<MainVM>(relaxed = true).also {
        every { it.imgPath } returns imgPath
    }
    private val frVMFactory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return frVM as T
            }
    }
    private val actVMFactory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return actVM as T
            }
    }
    private val uploadComponent = buildUploadComponent()

    @Before
    fun setUp() {
        App.INSTANCE.uploadComponent = uploadComponent
        fragment = ImgUploadFr()
        rule.launchFragment(fragment)
    }

    @After
    fun tearDown() {
        rule.finishActivity()
    }

    @Test
    fun checkUiElementsInitStates() {
        onView(withId(R.id.iv_image)).check(matches(isDisplayed()))
        onView(withId(R.id.et_tag)).check(matches(isDisplayed()))
        onView(withId(R.id.fab_upload)).check(matches(isDisplayed()))
        onView(withId(R.id.progress)).check(matches(not(isDisplayed())))
    }

    @Test
    fun testUploadOk() {
        val tag = "tag for the image"
        onView(withId(R.id.et_tag)).perform(typeText(tag))
        onView(withId(R.id.fab_upload)).perform(click())

        onView(withId(R.id.progress)).check(matches(isDisplayed()))
        verify { frVM.upload(imgPath, tag) }
        liveData.postValue("")
        onView(withId(R.id.progress)).check(matches(not(isDisplayed())))
    }

    @Test
    fun testUploadErr() {
        onView(withId(R.id.fab_upload)).perform(click())

        onView(withId(R.id.progress)).check(matches(isDisplayed()))
        liveData.postValue("error!!!")
        onView(withId(R.id.progress)).check(matches(not(isDisplayed())))
    }

    private fun buildUploadComponent(): UploadComponent {
        return DaggerUploadComponent.builder()
            .appComponent(mockk(relaxed = true))
            .mainModule(mockk<MainModule>(relaxed = true).also {
                every { it.provideActFact(any()) } returns actVMFactory
            })
            .uploadModule(mockk<UploadModule>(relaxed = true).also {
                every { it.provideUploadFact(any()) } returns frVMFactory
            })
            .build()
    }
}
