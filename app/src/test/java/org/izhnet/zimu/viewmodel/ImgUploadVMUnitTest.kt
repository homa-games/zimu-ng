package org.izhnet.zimu.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import org.izhnet.zimu.model.repository.Repository
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Пробные юнит тесты для [ImgUploadVM]
 */
class ImgUploadVMUnitTest {
    @get:Rule
    val rule = InstantTaskExecutorRule() //выполнение всех операций архитектурных компонентов в главном потоке

    private val repository = mockk<Repository>(relaxed = true)
    private lateinit var viewModel: ImgUploadVM

    @Before
    fun setUp() {
        viewModel = ImgUploadVM(repository)
    }

    @Test
    fun testInitState() {
        Assert.assertEquals(null, viewModel.getLiveData().value)
    }

    @Test
    fun testUploadOk() {
        coEvery { repository.uploadImg(any(), any()) }
                .coAnswers { coroutineScope { async { Unit } } }

        GlobalScope.launch {
            viewModel.upload("", "")
            Assert.assertEquals("", viewModel.getLiveData().value)
        }
    }

    @Test
    fun testUploadError() {
        val message = "upload error"
        coEvery { repository.uploadImg(any(), any()) } throws Throwable(message)

        GlobalScope.launch {
            viewModel.upload("", "")
            Assert.assertEquals(message, viewModel.getLiveData().value)
        }
    }
}