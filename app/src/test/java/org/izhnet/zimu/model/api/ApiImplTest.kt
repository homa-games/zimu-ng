package org.izhnet.zimu.model.api

import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.izhnet.zimu.ContextUtils
import org.izhnet.zimu.model.data.JsonItem
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File

/**
 * Тесты для [ApiImpl]
 */
class ApiImplTest {
    lateinit var api: IApi
    lateinit var apiImpl: ApiImpl
    lateinit var server: MockWebServer
    lateinit var file: File
    private val utils = mockk<ContextUtils>(relaxed = true).also {
        every { it.isNetworkConnected() } returns true
    }

    @Before
    fun setUp() {
        server = MockWebServer()
        api = Retrofit.Builder().baseUrl(server.url("/").toString())
                                .client(OkHttpClient())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build()
                                .create(IApi::class.java)
        apiImpl = ApiImpl(api, utils)
        file = File.createTempFile("test", ".png")
    }

    @After
    fun tearDown() {
        file.delete()
    }

    @Test
    fun testOK() {
        val json = JsonItem("OK", "jpg", "x9g.jpg", "https://example.com/x9g.jpg",
                            "https://example.com", "z48")
        val response: MockResponse = MockResponse().apply {
            setBody("""{"status":"OK","type":"jpg","hash":"x9g.jpg","url":"https:\/\/example.com\/x9g.jpg","domain":"https:\/\/example.com","deletecode":"z48"}""")
            setResponseCode(200)
        }
        server.enqueue(response)

        runBlocking {
            val item = apiImpl.upload(file).await()
            Assert.assertEquals(item, json)
        }
    }

    @Test
    fun testNullDeleteCode() {
        val json = JsonItem("OK", "jpg", "x9g.jpg", "https://example.com/x9g.jpg",
                            "https://example.com", null)
        val response: MockResponse = MockResponse().apply {
            setBody("""{"status":"OK","type":"jpg","hash":"x9g.jpg","url":"https:\/\/example.com\/x9g.jpg","domain":"https:\/\/example.com"}""")
            setResponseCode(200)
        }
        server.enqueue(response)

        runBlocking {
            val item = apiImpl.upload(file).await()
            Assert.assertEquals(json, item)
        }
    }

    @Test
    fun testErr() {
        val response: MockResponse = MockResponse().apply {
            setBody("""{"status":"ERR","reason":"NO_VALID_COMMAND"}""")
            setResponseCode(200)
        }
        server.enqueue(response)

        runBlocking {
            try {
                apiImpl.upload(file).await()
            } catch (e: Exception) {
                Assert.assertEquals("NO_VALID_COMMAND", e.message)
            }
        }
    }

    @Test
    fun testWTF() {
        val response: MockResponse = MockResponse().apply {
            setBody("""{"title":"wtf?!"}""")
            setResponseCode(200)
        }
        server.enqueue(response)

        runBlocking {
            try {
                apiImpl.upload(file).await()
            } catch (t: Throwable) {
                Assert.assertEquals("the response body not exist status field", t.message)
            }
        }
    }

    @Test
    fun testConvertErr() {
        val response: MockResponse = MockResponse().apply {
            setBody("""{wtf?!"""")
            setResponseCode(200)
        }
        server.enqueue(response)

        runBlocking {
            try {
                apiImpl.upload(file).await()
            } catch (t: Throwable) {
                Assert.assertTrue(t.message!!.isNotEmpty())
            }
        }
    }

    @Test
    fun testOffline() {
        every { utils.isNetworkConnected() } returns false

        runBlocking {
            try {
                apiImpl.upload(file).await()
            } catch (t: Throwable) {
                Assert.assertEquals("Нет подключения к сети интернет", t.message)
            }
        }
    }
}
