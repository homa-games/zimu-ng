package org.izhnet.zimu.model.repository

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Flowable
import io.reactivex.Single
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import org.izhnet.zimu.model.api.ApiImpl
import org.izhnet.zimu.model.data.DbItem
import org.izhnet.zimu.model.data.JsonItem
import org.izhnet.zimu.model.database.LinksDao
import org.junit.Assert
import org.junit.Test

/**
 * Тесты для [Repository]
 */
class RepositoryTest {
    private val apiImpl = mockk<ApiImpl>(relaxed = true)
    private val linksDao = mockk<LinksDao>(relaxed = true)
    private val repository = Repository(apiImpl, linksDao)
    private val path = "file://path"
    private val tag = "test tag"
    private val json1 = JsonItem("OK", "jpg", "x1g.jpg", "https://example.com/x1g.jpg",
                                 "https://example.com", "z01")
    private val dbItem1 = DbItem("jpg", "x1g.jpg", "https://example.com/x1g.jpg",
                                 "https://example.com", "z01", path, tag)
    private val dbItem2 = DbItem("jpg", "x2g.jpg", "https://example.com/x2g.jpg",
                                 "https://example.com", "z02", path, tag)

    @Test
    fun uploadImg() {
        coEvery { apiImpl.upload(any()) } coAnswers { coroutineScope { async { json1 } } }
        every { linksDao.insert(any()) } returns Single.just(1L)

        runBlocking {
            repository.uploadImg(path, tag).join()
            coVerify { linksDao.insert(dbItem1) }
        }
    }

    @Test
    fun uploadImgApiError() {
        val throwable = Throwable("api error")
        coEvery { apiImpl.upload(any()) } throws throwable
        every { linksDao.insert(any()) } returns Single.just(1L)

        runBlocking {
            try {
                repository.uploadImg(path, tag).join()
            } catch (t: Throwable) {
                Assert.assertEquals(throwable.message, t.message)
            }
            coVerify(exactly = 0) { linksDao.insert(dbItem1) }
        }
    }

    @Test
    fun uploadImgDbError() {
        val throwable = Throwable("db error")
        coEvery { apiImpl.upload(any()) } coAnswers { coroutineScope { async { json1 } } }
        every { linksDao.insert(any()) } returns Single.error(throwable)

        runBlocking {
            try {
                repository.uploadImg(path, tag).join()
            } catch (t: Throwable) {
                Assert.assertEquals(throwable.message, t.message)
            }
            coVerify { linksDao.insert(dbItem1) }
        }
    }

    @Test
    fun testGetPreviewList() {
        every { linksDao.getImgList() } returns Flowable.just(listOf(dbItem2), listOf(dbItem1, dbItem2))

        val subscriber = repository.getPreviewList().test()
        with(subscriber) {
            assertValueAt(0) { l -> l[0] == dbItem2 }
            assertValueAt(1) { l -> l[0] == dbItem1 && l[1] == dbItem2}
            assertNoErrors()
            assertComplete()
        }
    }

    @Test
    fun testGetPreviewListError() {
        val throwable = Throwable("test error")
        every { linksDao.getImgList() } returns Flowable.error(throwable)

        val subscriber = repository.getPreviewList().test()
        with(subscriber) {
            assertNoValues()
            assertError(throwable)
            assertNotComplete()
        }
    }

    @Test
    fun testGetImgInfo() {
        val hash = "x1g.jpg"
        every { linksDao.getImgInfo(hash) } returns Single.just(dbItem1)

        val observer = repository.getImgInfo(hash).test()
        with(observer) {
            assertValue(dbItem1)
            assertNoErrors()
            assertComplete()
        }
    }

    @Test
    fun testGetImgInfoError() {
        val hash = "x1g.jpg"
        val throwable = Throwable("test error")
        every { linksDao.getImgInfo(hash) } returns Single.error(throwable)

        val observer = repository.getImgInfo(hash).test()
        with(observer) {
            assertNoValues()
            assertError(throwable)
            assertNotComplete()
        }
    }

    @Test
    fun testUpdateImgInfo() {
        val single = Single.just(1)
        every { linksDao.update(dbItem1) } returns single

        val observer = single.test()
        repository.updateImgInfo(dbItem1)
        with(observer) {
            assertNoErrors()
            assertComplete()
        }
    }

    @Test
    fun testUpdateImgInfoError() {
        val throwable = Throwable("test error")
        val single = Single.error<Int>(throwable)
        every { linksDao.update(dbItem1) } returns single

        repository.updateImgInfo(dbItem1)
        val observer = single.test()
        with(observer) {
            assertError(throwable)
            assertNotComplete()
        }
    }
}