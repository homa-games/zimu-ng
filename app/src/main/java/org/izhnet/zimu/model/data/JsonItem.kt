package org.izhnet.zimu.model.data

/**
 * моделька для сетевого запроса
 */
data class JsonItem(val status: String, val type: String, val hash: String,
                    val url: String, val domain: String, val deletecode: String?) {
    fun toDb() : DbItem {
        return DbItem(type, hash, url, domain, deletecode ?: "")
    }
}
