package org.izhnet.zimu.model.api

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParseException
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.izhnet.zimu.ContextUtils
import org.izhnet.zimu.model.data.JsonItem
import retrofit2.Response
import java.io.File
import javax.inject.Inject

/**
 * Реализация методов серверного api
 */
class ApiImpl @Inject constructor(private val api: IApi, private val utils: ContextUtils) {

    suspend fun upload(file: File): Deferred<JsonItem> = coroutineScope {
        if (utils.isNetworkConnected()) {
            async(Dispatchers.IO) {
                val body = RequestBody.create(MediaType.parse("image/*"), file)
                val part = MultipartBody.Part.createFormData("postimage", file.name, body)
                parse(api.upload(part))
            }
        } else {
            throw Throwable("Нет подключения к сети интернет")
        }
    }

    @Throws(JsonParseException::class)
    fun parse(body: Response<JsonObject>): JsonItem {
        val b: JsonObject = body.body()
            ?: throw JsonParseException("the response body is null")
        val status: String = b.get("status")?.asString
            ?: throw JsonParseException("the response body not exist status field")

        return when (status) {
            "OK" -> Gson().fromJson(b.asJsonObject, JsonItem::class.java)
            "ERR" -> throw JsonParseException(b.get("reason").asString)
            else -> throw JsonParseException("can't parse server response")
        }
    }
}
