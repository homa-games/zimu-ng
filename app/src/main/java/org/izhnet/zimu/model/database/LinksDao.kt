package org.izhnet.zimu.model.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Update
import io.reactivex.Flowable
import io.reactivex.Single
import org.izhnet.zimu.model.data.DbItem

/**
 * Интерфейс для запросов к БД через room
 */
@Dao
interface LinksDao {
    @Insert(onConflict = IGNORE)
    fun insert(link: DbItem): Single<Long>

    @Update(onConflict = REPLACE)
    fun update(link: DbItem): Single<Int>

    @Query("SELECT * FROM links")
    fun getImgList(): Flowable<List<DbItem>>

    @Query("SELECT * FROM links WHERE _id=:hash")
    fun getImgInfo(hash: String): Single<DbItem>
}
