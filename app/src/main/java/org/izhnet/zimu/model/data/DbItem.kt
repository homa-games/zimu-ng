package org.izhnet.zimu.model.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * моделька для БД
 */
@Entity(tableName = "links")
data class DbItem(val type: String,
                  @PrimaryKey @ColumnInfo(name = "_id") val hash: String,
                  @ColumnInfo(name = "link") val url: String,
                  val domain: String,
                  val deletecode: String,
                  @ColumnInfo(name = "img_src") var imgSrc: String = "",
                  var tag: String = "")
