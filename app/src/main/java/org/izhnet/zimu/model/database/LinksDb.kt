package org.izhnet.zimu.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import org.izhnet.zimu.model.data.DbItem

/**
 * Класс БД для room
 */

@Database(entities = arrayOf(DbItem::class), version = 1, exportSchema = false)
abstract class LinksDb : RoomDatabase() {
    companion object {
        const val DB_NAME = "main_uploader.db"
    }
    abstract fun getDAO() : LinksDao
}
