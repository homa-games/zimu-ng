package org.izhnet.zimu.model.api

import com.google.gson.JsonObject
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

/**
 * Интерфейс серверного api
 */
interface IApi {
    @Multipart
    @POST("backend.php")
    suspend fun upload(@Part file: MultipartBody.Part): Response<JsonObject>
}
