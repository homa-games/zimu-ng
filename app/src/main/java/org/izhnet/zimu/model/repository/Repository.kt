package org.izhnet.zimu.model.repository

import android.util.Log
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import org.izhnet.zimu.model.api.ApiImpl
import org.izhnet.zimu.model.data.DbItem
import org.izhnet.zimu.model.database.LinksDao
import java.io.File
import javax.inject.Inject

/**
 * Репозиторий для загруженных картинок
 */
class Repository @Inject constructor(private val apiImpl: ApiImpl,
                                     private val linksDao: LinksDao) {
    suspend fun uploadImg(path: String, tag: String = ""): Job = coroutineScope {
        async(Dispatchers.IO) {
            val item = apiImpl.upload(File(path)).await()
                                                 .toDb().also { it.imgSrc = path
                                                                it.tag = tag }
            linksDao.insert(item).subscribeOn(Schedulers.io()).subscribe({}, {})
        }
    }

    fun getPreviewList(): Flowable<List<DbItem>> {
        return linksDao.getImgList()
    }

    fun getImgInfo(hash: String): Single<DbItem> {
        return linksDao.getImgInfo(hash)
    }

    fun updateImgInfo(item: DbItem) {
        lateinit var d: Disposable
        d = linksDao.update(item).subscribeOn(Schedulers.io())
                                 .subscribeOn(Schedulers.io())
                                 .subscribe( { d.dispose() }, { Log.e("123", it.message) })
    }
}
