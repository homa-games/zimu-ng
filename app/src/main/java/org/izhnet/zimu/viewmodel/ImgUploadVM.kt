package org.izhnet.zimu.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.izhnet.zimu.annotation.OpenForTest
import org.izhnet.zimu.model.repository.Repository
import javax.inject.Inject

/**
 * [ViewModel] для [org.izhnet.zimu.ImgUploadFr]
 */
@OpenForTest
class ImgUploadVM @Inject constructor(private val repository: Repository) : ViewModel() {
    private val liveData = MutableLiveData<String>()

    fun getLiveData(): LiveData<String> = liveData

    fun upload(file: String, tag: String) {
        val handler = CoroutineExceptionHandler {_, t ->
            liveData.postValue(t.message)
        }
        GlobalScope.launch(Dispatchers.Main + handler) {
            withContext(Dispatchers.IO) { repository.uploadImg(file, tag) }.join()
            liveData.postValue("")
        }
    }

    override fun onCleared() {
    }
}
