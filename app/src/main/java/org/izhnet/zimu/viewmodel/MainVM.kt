package org.izhnet.zimu.viewmodel

import androidx.lifecycle.ViewModel
import org.izhnet.zimu.annotation.OpenForTest
import org.izhnet.zimu.model.data.DbItem

/**
 * [ViewModel] для [org.izhnet.zimu.MainActivity]
 */
@OpenForTest
class MainVM : ViewModel() {
    var imgPath = ""
    lateinit var item: DbItem

    override fun onCleared() {
    }
}