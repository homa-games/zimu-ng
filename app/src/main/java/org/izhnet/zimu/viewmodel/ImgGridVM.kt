package org.izhnet.zimu.viewmodel

import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.core.content.FileProvider
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import org.izhnet.zimu.R
import org.izhnet.zimu.annotation.OpenForTest
import org.izhnet.zimu.model.data.DbItem
import org.izhnet.zimu.model.repository.Repository
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * [ViewModel] для [org.izhnet.zimu.ImgGridFr]
 */
@OpenForTest
class ImgGridVM @Inject constructor(private val ctx: Context, repository: Repository) : ViewModel() {
    private val liveData = MutableLiveData<List<DbItem>>()
    private val disp = CompositeDisposable()

    init {
        repository.getPreviewList().subscribeOn(Schedulers.io())
                                   .observeOn(Schedulers.io())
                                   .subscribe({ liveData.postValue(it) },
                                              { Log.e("123", it.message) })
                                   .addTo(disp)
    }

    override fun onCleared() {
        disp.dispose()
    }

    fun getLiveData(): LiveData<List<DbItem>> = liveData

    fun getSize(): Int {
        return liveData.value?.size ?: 0
    }

    fun getPreview(position: Int): Uri {
        val path = liveData.value?.get(position)?.imgSrc

        return if (path != null && File(path).exists()) {
            Uri.parse(path)
        } else {
            Uri.parse("android.resource://" + ctx.packageName + "/" + R.drawable.ic_launcher_foreground)
        }
    }

    fun getItem(position: Int): DbItem? {
        return liveData.value?.get(position)
    }

    fun getImgPath(uri: Uri): String {
        val cursor = ctx.contentResolver.query(uri, arrayOf(MediaStore.Images.Media.DATA),
                                               null, null, null)!!
        val index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val path = cursor.getString(index)
        cursor.close()
        return path
    }

    fun createImageFile(): File? {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        return try {
            val dir: File = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
            File(dir, "/Camera/IMG_$timeStamp.jpg")
        } catch (e: Exception) {
            null
        }
    }

    fun createUriFromFile(file: File): Uri {
        return if (Build.VERSION.SDK_INT >= 21) {
            FileProvider.getUriForFile(ctx, ctx.packageName + ".fileprovider", file)
        } else {
            Uri.fromFile(file)
        }
    }
}
