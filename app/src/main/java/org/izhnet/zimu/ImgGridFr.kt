package org.izhnet.zimu

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.izhnet.zimu.viewmodel.ImgGridVM
import javax.inject.Inject
import javax.inject.Named

/**
 * Фрагмент с превью всех загруженных картинок
 */
class ImgGridFr : CommonFragment() {
    companion object {
        const val CAMERA = 0 //загрузка снимка с камеры
        const val GALLERY = 1 //загрузка из галереи
        const val COLUMN = 2 //число столбцов в таблице загруженных изображений

        const val REQUEST_CAMERA = 0 //запрос на доступ к камере
        const val REQUEST_STORAGE = 1 //запрос на доступ к хранилищу
    }

    @field:[Inject Named("grid")]
    lateinit var frFactory: ViewModelProvider.Factory

    private lateinit var imgGrid: RecyclerView
    private lateinit var fabCamera: FloatingActionButton
    private lateinit var fabGallery: FloatingActionButton
    private lateinit var imgGridVM: ImgGridVM

    private val imgAdapter = ImgAdapter()
    private var photoPath = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        App.INSTANCE.gridComponent?.inject(this)
        imgGridVM = ViewModelProviders.of(this, frFactory).get(ImgGridVM::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.image_grid_fragment, container, false)

        imgGrid = view.findViewById(R.id.rv_image_grid)
        fabCamera = view.findViewById(R.id.fab_camera)
        fabGallery = view.findViewById(R.id.fab_gallery)

        fabCamera.setOnClickListener {
            if (isPermissionGranted(Manifest.permission.CAMERA, REQUEST_CAMERA)
                && isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE, REQUEST_STORAGE)) {
                onCameraButtonClick()
            } }
        fabGallery.setOnClickListener { onGalleryButtonClick() }

        imgGrid.adapter = imgAdapter
        imgGrid.layoutManager = GridLayoutManager(activity, COLUMN)

        imgGridVM.getLiveData().observe(this, Observer { imgAdapter.notifyDataSetChanged() })

        return view
    }

    private fun isPermissionGranted(permission: String, requestCode: Int): Boolean =
            if (ContextCompat.checkSelfPermission(activity!!, permission)
                    == PermissionChecker.PERMISSION_GRANTED) {
                true
            } else {
                if (shouldShowRequestPermissionRationale(permission)) {
                    //нужно показать обоснование
                    showToast("application needs permission: $permission")
                } else {
                    //нужно запросить разрешение
                    requestPermissions(arrayOf(permission), requestCode)
                }
                false
            }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) return
        when (requestCode) {
            CAMERA -> {mainVM.imgPath = photoPath}
            GALLERY -> {mainVM.imgPath = imgGridVM.getImgPath(data!!.data!!)}
        }
        navigator?.navigate(R.id.imgUploadFr)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CAMERA -> {
                if (grantResults.isNotEmpty()
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onCameraButtonClick()
                } else {
                    //разрешение не получено
                }
                return
            }
            else -> {}
        }
    }

    private fun onCameraButtonClick() {
        val img = imgGridVM.createImageFile()
        if (img != null) {
            photoPath = img.absolutePath
            val uri = imgGridVM.createUriFromFile(img)
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE).apply {
                                putExtra(MediaStore.EXTRA_OUTPUT, uri)}
            startActivityForResult(intent, CAMERA)
        } else {
            showToast("Can't create photo file")
            return //если фотки нет, то и загружать нечего
        }
    }

    private fun onGalleryButtonClick() {
        val intent = Intent(Intent.ACTION_PICK).apply {
            setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                           MediaStore.Images.Media.CONTENT_TYPE)
        }
        startActivityForResult(intent, GALLERY)
    }

    private inner class ImgAdapter : RecyclerView.Adapter<ImgViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImgViewHolder {
            return ImgViewHolder(LayoutInflater.from(parent.context)
                                               .inflate(R.layout.image_card, parent, false))
        }

        override fun getItemCount(): Int {
            return imgGridVM.getSize()
        }

        override fun onBindViewHolder(holder: ImgViewHolder, position: Int) {
            holder.view.apply {
                setImageURI(imgGridVM.getPreview(position))
                setOnClickListener {
                    mainVM.item = imgGridVM.getItem(position)!!
                    navigator?.navigate(R.id.imgShareFr) }
            }
        }
    }

    private inner class ImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val view: ImageView = itemView.findViewById(R.id.iv_image)
    }
}
