package org.izhnet.zimu.dagger.module

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import org.izhnet.zimu.annotation.OpenForTest
import org.izhnet.zimu.App
import javax.inject.Named

/**
 * [Module] для получения зависимостей [org.izhnet.zimu.MainActivity]
 */
@OpenForTest
@Module
class MainModule {
    @Provides
    @Named("main")
    fun provideActFact(app: App): ViewModelProvider.Factory {
        return ViewModelProvider.AndroidViewModelFactory(app)
    }
}
