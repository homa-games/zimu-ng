package org.izhnet.zimu.dagger.module.singletone

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import org.izhnet.zimu.model.database.LinksDao
import org.izhnet.zimu.model.database.LinksDb
import org.izhnet.zimu.model.database.LinksDb.Companion.DB_NAME
import javax.inject.Singleton

/**
 * [Module] для получения экземпляра [LinksDao]
 */
@Module
class DaoModule {
    @Provides
    @Singleton
    fun provideDao(cnt: Context): LinksDao {
        return Room.databaseBuilder(cnt, LinksDb::class.java, DB_NAME).build().getDAO()
    }
}
