package org.izhnet.zimu.dagger

import dagger.Component
import org.izhnet.zimu.ImgGridFr
import org.izhnet.zimu.dagger.module.GridModule
import org.izhnet.zimu.dagger.module.MainModule

/**
 * [Component] для зависимостей [org.izhnet.zimu.ImgGridFr]
 */
@Component(dependencies = [AppComponent::class],
           modules = [GridModule::class, MainModule::class])
@GridScope
interface GridComponent {
    fun inject(fragment: ImgGridFr)
}
