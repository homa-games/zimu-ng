package org.izhnet.zimu.dagger

import dagger.Component
import org.izhnet.zimu.ImgUploadFr
import org.izhnet.zimu.dagger.module.MainModule
import org.izhnet.zimu.dagger.module.UploadModule

/**
 * [Component] для зависимостей [org.izhnet.zimu.ImgGridFr]
 */
@Component(dependencies = [AppComponent::class],
           modules = [UploadModule::class, MainModule::class])
@UploadScope
interface UploadComponent {
    fun inject(fragment: ImgUploadFr)
}
