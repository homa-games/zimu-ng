package org.izhnet.zimu.dagger

import android.content.Context
import dagger.Component
import org.izhnet.zimu.dagger.module.singletone.ApiModule
import org.izhnet.zimu.dagger.module.singletone.AppModule
import org.izhnet.zimu.dagger.module.singletone.DaoModule
import org.izhnet.zimu.App
import org.izhnet.zimu.model.api.IApi
import org.izhnet.zimu.model.database.LinksDao
import javax.inject.Singleton

/**
 * [Component] зависимостей уровня приложения
 */
@Component(modules = [ApiModule::class, AppModule::class, DaoModule::class])
@Singleton
interface AppComponent {
    fun getApi(): IApi
    fun getContext(): Context
    fun getApp(): App
    fun getDao(): LinksDao
}
