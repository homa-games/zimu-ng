package org.izhnet.zimu.dagger

import dagger.Component
import org.izhnet.zimu.MainActivity
import org.izhnet.zimu.dagger.module.MainModule

/**
 * [Component] для зависимостей [MainActivity]
 */
@Component(dependencies = [AppComponent::class], modules = [MainModule::class])
@MainScope
interface MainComponent {
    fun inject(activity: MainActivity)
}
