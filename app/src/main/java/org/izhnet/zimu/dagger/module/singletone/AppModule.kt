package org.izhnet.zimu.dagger.module.singletone

import android.content.Context
import dagger.Module
import dagger.Provides
import org.izhnet.zimu.App
import javax.inject.Singleton

/**
 * [Module] для получения экземпляра приложения [App] и его [Context]
 */
@Module
class AppModule(private val app: App) {
    @Provides
    @Singleton
    fun provideApp(): App {
        return app
    }

    @Provides
    @Singleton
    fun provideAppContext(): Context {
        return app.applicationContext
    }
}
