package org.izhnet.zimu.dagger

import dagger.Component
import org.izhnet.zimu.ImgShareFr
import org.izhnet.zimu.dagger.module.MainModule

/**
 * [Component] для зависимостей [org.izhnet.zimu.ImgShareFr]
 */
@Component(dependencies = [AppComponent::class],
           modules = [MainModule::class])
@ShareScope
interface ShareComponent {
    fun inject(fragment: ImgShareFr)
}
