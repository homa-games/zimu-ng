package org.izhnet.zimu.dagger.module

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import org.izhnet.zimu.annotation.OpenForTest
import org.izhnet.zimu.dagger.GridScope
import org.izhnet.zimu.model.repository.Repository
import org.izhnet.zimu.viewmodel.ImgGridVM
import javax.inject.Named

/**
 * [Module] для получения зависимостей [org.izhnet.zimu.ImgGridFr]
 */
@OpenForTest
@Module
class GridModule {
    @Suppress("UNCHECKED_CAST")
    @Provides
    @GridScope
    @Named("grid")
    fun provideGridFact(ctx: Context, repository: Repository): ViewModelProvider.Factory {
        return object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return if (modelClass.isAssignableFrom(ImgGridVM::class.java)) {
                    ImgGridVM(ctx, repository) as T
                } else {
                    throw IllegalArgumentException("ViewModel Not Found")
                }
            }
        }
    }
}
