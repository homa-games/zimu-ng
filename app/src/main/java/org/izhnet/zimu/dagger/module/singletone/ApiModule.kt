package org.izhnet.zimu.dagger.module.singletone

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.izhnet.zimu.BuildConfig
import org.izhnet.zimu.model.api.IApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * [Module] для получения экземпляра [IApi]
 */
@Module
class ApiModule {
    @Provides
    @Singleton
    fun provideClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
    }

    @Provides
    @Singleton
    fun provideApi(client: OkHttpClient): IApi {
        return Retrofit.Builder().baseUrl(BuildConfig.URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(IApi::class.java)
    }
}
