package org.izhnet.zimu.dagger.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import org.izhnet.zimu.annotation.OpenForTest
import org.izhnet.zimu.dagger.UploadScope
import org.izhnet.zimu.model.repository.Repository
import org.izhnet.zimu.viewmodel.ImgUploadVM
import javax.inject.Named

/**
 * [Module] для получения зависимостей [org.izhnet.zimu.ImgUploadFr]
 */
@OpenForTest
@Module
class UploadModule {
    @Suppress("UNCHECKED_CAST")
    @Provides
    @UploadScope
    @Named("upload")
    fun provideUploadFact(repository: Repository): ViewModelProvider.Factory {
        return object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return if (modelClass.isAssignableFrom(ImgUploadVM::class.java)) {
                    ImgUploadVM(repository) as T
                } else {
                    throw IllegalArgumentException("ViewModel Not Found")
                }
            }
        }
    }
}
