package org.izhnet.zimu.dagger

import javax.inject.Scope

/**
 * Дополнительные [Scope] аннотации
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class MainScope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class GridScope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class UploadScope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ShareScope
