package org.izhnet.zimu

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.izhnet.zimu.viewmodel.ImgUploadVM
import javax.inject.Inject
import javax.inject.Named

/**
 * Фрагмент для предпросмотра и редактирования загружаемой картинки
 */
class ImgUploadFr : CommonFragment() {
    @field:[Inject Named("upload")]
    lateinit var frFactory: ViewModelProvider.Factory

    private lateinit var progressBar: ProgressBar
    private lateinit var imageView: ImageView
    private lateinit var editText: EditText
    private lateinit var fabUpload: FloatingActionButton
    private lateinit var imgUploadVM: ImgUploadVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        App.INSTANCE.uploadComponent?.inject(this)
        imgUploadVM = ViewModelProviders.of(this, frFactory).get(ImgUploadVM::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.image_upload_fragment, container, false)
        with(view) {
            progressBar = findViewById(R.id.progress)
            imageView = findViewById(R.id.iv_image)
            editText = findViewById(R.id.et_tag)
            fabUpload = findViewById(R.id.fab_upload)
        }
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        fabUpload.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            imgUploadVM.upload(mainVM.imgPath, editText.text.toString())
        }
        imgUploadVM.getLiveData().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.isEmpty()) {
                     showToast("Файл успешно загружен")
                 } else {
                     Log.e("123", it)
                     showToast(it)
                 }
        })
        imageView.setImageURI(Uri.parse(mainVM.imgPath))
    }
}
