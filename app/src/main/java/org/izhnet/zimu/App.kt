package org.izhnet.zimu

import android.app.Application
import android.widget.Toast
import androidx.annotation.StringRes
import org.izhnet.zimu.dagger.AppComponent
import org.izhnet.zimu.dagger.DaggerAppComponent
import org.izhnet.zimu.dagger.DaggerGridComponent
import org.izhnet.zimu.dagger.DaggerMainComponent
import org.izhnet.zimu.dagger.DaggerShareComponent
import org.izhnet.zimu.dagger.DaggerUploadComponent
import org.izhnet.zimu.dagger.GridComponent
import org.izhnet.zimu.dagger.MainComponent
import org.izhnet.zimu.dagger.ShareComponent
import org.izhnet.zimu.dagger.UploadComponent
import org.izhnet.zimu.dagger.module.GridModule
import org.izhnet.zimu.dagger.module.MainModule
import org.izhnet.zimu.dagger.module.UploadModule
import org.izhnet.zimu.dagger.module.singletone.ApiModule
import org.izhnet.zimu.dagger.module.singletone.AppModule
import org.izhnet.zimu.dagger.module.singletone.DaoModule
import org.izhnet.zimu.logger.CustomExceptionHandler

/**
 * Класс приложения
 */
class App : Application() {
    var appComponent: AppComponent? = null
        get() {
            if (field == null) {
                field = buildAppComponent()
            }
            return field
        }
    var mainComponent: MainComponent? = null
        get() {
            if (field == null) {
                field = buildMainComponent()
            }
            return field
        }
    var gridComponent: GridComponent? = null
        get() {
            if (field == null) {
                field = buildGridComponent()
            }
            return field
        }
    var shareComponent: ShareComponent? = null
        get() {
            if (field == null) {
                field = buildShareComponent()
            }
            return field
        }
    var uploadComponent: UploadComponent? = null
        get() {
            if (field == null) {
                field = buildUploadComponent()
            }
            return field
        }

    companion object {
        lateinit var INSTANCE: App
            private set

        fun showToast(@StringRes resId: Int) {
            Toast.makeText(INSTANCE, resId, Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreate() {
        super.onCreate()

        // запись логов падений в файл
        if (BuildConfig.DEBUG) {
            logging()
        }
        INSTANCE = this
    }

    private fun buildAppComponent(): AppComponent {
        return DaggerAppComponent.builder()
            .apiModule(ApiModule())
            .appModule(AppModule(this))
            .daoModule(DaoModule())
            .build()
    }

    private fun buildMainComponent(): MainComponent {
        return DaggerMainComponent.builder()
            .appComponent(appComponent)
            .mainModule(MainModule())
            .build()
    }

    private fun buildGridComponent(): GridComponent {
        return DaggerGridComponent.builder()
            .appComponent(appComponent)
            .gridModule(GridModule())
            .build()
    }

    private fun buildShareComponent(): ShareComponent {
        return DaggerShareComponent.builder()
            .appComponent(appComponent)
            .build()
    }

    private fun buildUploadComponent(): UploadComponent {
        return DaggerUploadComponent.builder()
            .appComponent(appComponent)
            .uploadModule(UploadModule())
            .build()
    }

    private fun logging() {
        val handler = CustomExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler(handler)
    }
}
