package org.izhnet.zimu.logger

import android.os.Environment
import java.io.File
import java.io.FileWriter
import java.io.PrintWriter

/**
 * Класс для логирования падений
 */
class CustomExceptionHandler : Thread.UncaughtExceptionHandler {
    private val oldHandler = Thread.getDefaultUncaughtExceptionHandler()

    override fun uncaughtException(t: Thread?, e: Throwable?) {
        try {
            logging(e!!)
        } catch (e: Exception) {
            // опаньки!
        }
        oldHandler.uncaughtException(t, e)
    }

    private fun logging(e: Throwable) {
        // логи пишутся в корень sd карты
        val logFile = File(Environment.getExternalStoragePublicDirectory(""),
                           "zimu_crash.log")
        if (!logFile.exists()) {
            logFile.createNewFile()
        }
        val fileWriter = FileWriter(logFile, true)
        val printWriter = PrintWriter(fileWriter, true)

        fileWriter.appendln("==== new stacktrace ====")
        e.printStackTrace(printWriter)
        fileWriter.appendln()

        printWriter.close()
        fileWriter.close()
    }
}
