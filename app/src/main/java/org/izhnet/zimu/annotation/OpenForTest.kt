package org.izhnet.zimu.annotation

/**
 * Аннотация, позволяющая при компиляции убирать модификатор final у классов и полей.
 * Необходимо что бы Mockito мог их мокать в тестах
 */
annotation class OpenForTest
