package org.izhnet.zimu

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import org.izhnet.zimu.viewmodel.MainVM
import javax.inject.Inject
import javax.inject.Named

class MainActivity : AppCompatActivity() {
    @field:[Inject Named("main")]
    lateinit var factory: ViewModelProvider.Factory
    private lateinit var navigator: NavController
    private lateinit var mainVM: MainVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        App.INSTANCE.mainComponent?.inject(this)

        navigator = Navigation.findNavController(this, R.id.nav_host_fragment)

        mainVM = ViewModelProviders.of(this, factory).get(MainVM::class.java)
    }
}
