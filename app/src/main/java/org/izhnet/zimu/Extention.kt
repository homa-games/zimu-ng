package org.izhnet.zimu

import android.app.Activity
import android.widget.Toast
import androidx.fragment.app.Fragment

/**
 * Функции-расширения
 */

fun Activity.showToast(msg: String, duration: Int = Toast.LENGTH_LONG) {
    runOnUiThread { Toast.makeText(this, msg, duration).show() }

}

fun Fragment.showToast(msg: String, duration: Int = Toast.LENGTH_LONG) {
    activity!!.runOnUiThread { Toast.makeText(activity, msg, duration).show() }
}
