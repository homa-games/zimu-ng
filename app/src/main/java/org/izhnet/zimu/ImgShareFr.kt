package org.izhnet.zimu

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import com.google.android.material.floatingactionbutton.FloatingActionButton

/**
 * Фрагмент для расшаривания картинки
 */
class ImgShareFr : CommonFragment() {
    companion object {
        const val CLIP_LABEL = "Image link"
    }

    private lateinit var image: ImageView
    private lateinit var comment: EditText
    private lateinit var fabBuffer: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        App.INSTANCE.shareComponent?.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.image_share_fragment, container, false)
        with(view) {
            image = findViewById(R.id.iv_image)
            comment = findViewById(R.id.et_comment)
            fabBuffer = findViewById(R.id.fab_buffer)
        }
        fabBuffer.setOnClickListener {copyToBuffer()}

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        image.setImageURI(Uri.parse(mainVM.item.imgSrc))
    }

    private fun copyToBuffer() {
        val clipboard = context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData: ClipData = ClipData.newPlainText(CLIP_LABEL, mainVM.item.url)
        clipboard.primaryClip = clipData

        showToast(getString(R.string.copy_to_buffer_msg))
    }
}
