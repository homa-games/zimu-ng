package org.izhnet.zimu

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import org.izhnet.zimu.viewmodel.MainVM
import javax.inject.Inject
import javax.inject.Named

/**
 * Базовый фрагмент
 */
open class CommonFragment : Fragment() {
    @field:[Inject Named("main")]
    lateinit var actFactory: ViewModelProvider.Factory
    protected lateinit var mainVM: MainVM
    var navigator: NavController? = null

    init {
        retainInstance = true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //чтобы не падало в тестах и можно было мокать контроллер
        try {
            navigator = Navigation.findNavController(view)
        } catch (e: Exception) {
            //nothing
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainVM = ViewModelProviders.of(activity!!, actFactory).get(MainVM::class.java)
    }
}
